from hashlib import pbkdf2_hmac
from typing import Optional

from Cryptodome.Cipher import AES
from sqlalchemy import TypeDecorator, String, create_engine
from sqlalchemy import select
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import sessionmaker


PASSWORD = b"password"
PEPPER = b"randomstaticsalt"
SECRET_KEY = pbkdf2_hmac("sha256", PASSWORD, PEPPER, 480000, 16)


class EncryptedString(TypeDecorator):
    # This is the type used to store our data in the database.
    # If your data was larger, you could also use the `sqlalchemy.LargeBinary` type instead of
    # `sqlalchemy.String` as your database representation.
    impl = String

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def process_bind_param(self, value: str, _dialect) -> Optional[str]:
        """
        This function is called for conversion of the python representation (str) into the database
        representation (sqlalchemy.String).
        As the conversion str->sqlalchemy.String can happen transparently, we just deal with
        encrypting our string here, and return it as a hex encoded string again.
        If your data was larger, you could also return `bytes` and use the `sqlalchemy.LargeBinary`
        type as your database representation.
        """
        if value is not None:
            cipher = AES.new(SECRET_KEY, AES.MODE_GCM)
            ciphertext, auth_tag = cipher.encrypt_and_digest(value.encode("utf-8"))
            nonce = cipher.nonce
            return (auth_tag + nonce + ciphertext).hex()
        else:
            return value

    def process_result_value(self, value: str, _dialect) -> Optional[str]:
        """
        This function is called for conversion from the database representation (sqlalchemy.String)
        to our python representation (str).
        As this conversion already happens transparently by sqlalchemy, we just deal with decrypting
        our value here.
        """
        if value is not None:
            bytes_value = bytes.fromhex(value)
            auth_tag, nonce, ciphertext = (
                bytes_value[:16],
                bytes_value[16:32],
                bytes_value[32:],
            )
            cipher = AES.new(SECRET_KEY, AES.MODE_GCM, nonce=nonce)
            plaintext = cipher.decrypt_and_verify(ciphertext, auth_tag)
            return plaintext.decode("utf-8")
        else:
            return value


class Base(DeclarativeBase):
    pass


class User(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(EncryptedString)


if __name__ == "__main__":
    engine = create_engine("sqlite:///test_db.sqlite3")
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)

    # Notice how all the encryption and decryption works transparently.
    # As a user of our DB model (and EncryptedString type) we don't have to deal with cryptography
    # at all.
    session = Session()
    user = User(name="username1")
    session.add(user)
    session.commit()
    queried_user = session.execute(select(User)).scalars().first()
    if queried_user is not None:
        print(queried_user)
        print(queried_user.id)
        print(queried_user.name)
