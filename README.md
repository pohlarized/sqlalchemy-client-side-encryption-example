# SQLAlchemy Client Encryption Example

This is a small example how you can create your own transparent encrypted datatypes / columns using SQLAlchemy in python, using `pycryptodomex` as the cryptography provider.
This is only for illustrative purposes how you can implement such transformations transparently with SQLAlchemy.
There is no guarantee that any of this code is secure (or even working) in any way.

To verify that the data is indeed encrypted in the database, you can inspect it with something like [sqlitebrowser](https://sqlitebrowser.org/).

Notice that with this architecture, you cannot query on the encrypted columns.
This is because proper encryption requires a random nonce for each column, which is stored alongside the encrypted value.
This prevents various unwanted effects, like the same value resulting in the same ciphertext when stored in two different rows.
However, this means that we would have to encrypt our search term once for every value we're comparing against (i.e. for every row in the table we're querying).
To query on encrypted columns, you have to resort to server-side encryption.
